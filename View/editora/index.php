<!doctype html>
<html lang="pt_BR">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css" integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

    <title>Cadastro de Editora | Sislib</title>
  </head>
  <body>
    <!-- NAVBAR -->
    <ul class="nav nav-pills nav-fill mt-2">
        <li class="nav-item">
            <a class="nav-link" href="#">Página Inicial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link active" href="view/editora/index.php">Editora</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Autor</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">Livro</a>
        </li>
    </ul>
    <!-- FIM NAVBAR -->

    <!-- FORM / CARD-->

    <div class="card mt-2">
        <div class="card-header">
            Novo Cadastro
        </div>
        <div class="card-body">
            <h5 class="card-title">Cadastro de Editoras</h5>
            <form>
                <div class="form-group">
                    <label for="EditoraNome">Nome da Editora</label>
                    <input type="text" class="form-control" id="EditoraNome">
                </div>
                <div class="form-group">
                    <label for="EditoraPais">País de Origem</label>
                    <input type="text" class="form-control" id="EditoraPais">
                </div>
            </form>
            <a href="#" class="btn btn-primary">Cadastrar</a>
        </div>
    </div>
    
    <!-- FIM FORM / CARD-->

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
  </body>
</html>